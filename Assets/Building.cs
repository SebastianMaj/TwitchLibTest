﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Building : MonoBehaviour 
{
	public TwitchPlayer owner;
	public int level = 1;
	public double cash = 0.0;
	private double cashGainPerSecond { get { return (double)(Mathf.Pow(Mathf.Log10(level), 5) + 1); } }
	public double upgradeCost { get { return (double)(Mathf.Pow(level, 3) + 14); } }
	public bool confirmStage = false;

	[SpaceAttribute]

	public Text ownerText;
	public Text levelText;
	public Text cashText;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine(updateTimer());
	}
	
	// Update is called once per frame
	void Update () 
	{
		cash += cashGainPerSecond*Time.deltaTime;
	}

	// Purchase Level
	protected void purchaseLevel()
	{
		if(cash >= upgradeCost)
		{
			cash -= upgradeCost;
			level++;
		}
	}

	// Start Confirm timer
	public void upgradeTimerStart()
	{
		StartCoroutine(upgradeConfirmTimer());
	}

	// Confirm upgradeCost
	public void confirmUpgrade()
	{
		purchaseLevel();
	}

	// Coroutine
	IEnumerator updateTimer()
	{
		while(true)
		{
			updateText();
			yield return new WaitForSeconds(5.0f);
		}
		yield return null;
	}

	// Confirm timer
	IEnumerator upgradeConfirmTimer()
	{
		confirmStage = true;
		yield return new WaitForSeconds(10.0f);
		confirmStage = false;
	}

	// Update Text
	public void updateText()
	{
		ownerText.text = string.Format("Owner: {0}", owner.userName);
		levelText.text = string.Format("Level: {0}", level);
		cashText.text = string.Format("Cash: {0}", cash.ToString("F2"));
	}
}
