﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions 
{
	public static GameObject InstantiateNow(this GameObject go)
	{
		return GameObject.Instantiate(go);
	}	

	public static string ColorCode(this Color c)
	{
		int r = (int)c.r; int g = (int)c.g; int b = (int)c.b;
		string nameColor = string.Format("#{0:X2}{1:X2}{2:X2}", r, g, b);
		return nameColor;
	}
}
