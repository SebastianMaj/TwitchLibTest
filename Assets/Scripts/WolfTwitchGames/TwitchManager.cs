using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(TwitchIRC))]
public class TwitchManager : MonoBehaviour
{
    public static TwitchManager instance;

    public int maxMessages = 100; //we start deleting UI elements when the count is larger than this var.
    private LinkedList<GameObject> messages =
        new LinkedList<GameObject>();
    private TwitchIRC IRC;

    void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    //when message is recieved from IRC-server or our own message.
    void OnChatMsgRecieved(string msg)
    {
        //parse from buffer.
        int msgIndex = msg.IndexOf("PRIVMSG #");
        string msgString = msg.Substring(msgIndex + IRC.yourLogin.channelName.Length + 11);
        string user = msg.Substring(1, msg.IndexOf('!') - 1);

        //remove old messages for performance reasons.
        if (messages.Count > maxMessages)
        {
            Destroy(messages.First.Value);
            messages.RemoveFirst();
        }

        string lowerMsgString = msgString.ToLower();
        if(lowerMsgString.Equals("!join"))
        {  
            TwitchPlayer newTwitchPlayer = new TwitchPlayer(user, ColorFromUsername(user));
            TwitchPlayerManager.instance.addPlayer(newTwitchPlayer);
        }
        else if(lowerMsgString.Equals("!leave"))
        {
            TwitchPlayer newTwitchPlayer = new TwitchPlayer(user, ColorFromUsername(user));
            TwitchPlayerManager.instance.removePlayer(newTwitchPlayer);
        }
        else if(lowerMsgString.Equals("!status"))
        {
            if(TwitchPlayerManager.isPlayerInGame(user))
            {
                TwitchPlayer yourPlayer = TwitchPlayerManager.getPlayer(user);

                if(yourPlayer.yourBuilding)
                {
                    string buildStatus = "{ ";
                    buildStatus += ("Level: " + yourPlayer.yourBuilding.level + " | ");
                    buildStatus += ("Cash: " + yourPlayer.yourBuilding.cash.ToString("F2") + " | ");
                    buildStatus += ("Upgrade Cost: " + yourPlayer.yourBuilding.upgradeCost.ToString("F2") + " | ");
                    buildStatus += " }";

                    sendMessage(string.Format("/w {0} Your buildings status is: {1}", user, buildStatus));
                }
                else
                {
                    sendMessage("/w " + user + " You do not have a building yet! Type !newbuilding in the channel chat to obtain a building.");
                }
            }
            else
            {
                sendMessage("/w " + user + " Join the game first by typing !join in the channel chat.");
            }
        }
        else if(lowerMsgString.Equals("!upgrade"))
        {
            if(TwitchPlayerManager.isPlayerInGame(user))
            {
                TwitchPlayer yourPlayer = TwitchPlayerManager.getPlayer(user);

                if(yourPlayer.yourBuilding)
                {
                    sendMessage(string.Format("{0} your building upgrade will cost you {1}. You have {2}. Type !confirm to purchase this upgrade.", yourPlayer.userName, yourPlayer.yourBuilding.upgradeCost.ToString("F2"), yourPlayer.yourBuilding.cash.ToString("F2")));
                    yourPlayer.yourBuilding.upgradeTimerStart();
                }
                else
                {
                    sendMessage("/w " + user + " You do not have a building yet! Type !newbuilding in the channel chat to obtain a building.");
                }
            }
            else
            {
                sendMessage("/w " + user + " Join the game first by typing !join in the channel chat.");
            }
        }
        else if(lowerMsgString.Equals("!confirm"))
        {
            if(TwitchPlayerManager.isPlayerInGame(user))
            {
                TwitchPlayer yourPlayer = TwitchPlayerManager.getPlayer(user);

                if(yourPlayer.yourBuilding)
                {
                    if(yourPlayer.yourBuilding.confirmStage)
                    {
                        yourPlayer.yourBuilding.confirmUpgrade();
                        sendMessage(string.Format("{0} your building has been upgraded! New balance: {1} | New Upgrade Cost: {2}", yourPlayer.userName, yourPlayer.yourBuilding.cash.ToString("F2"), yourPlayer.yourBuilding.upgradeCost.ToString("F2")));
                    }
                    else
                    {
                        sendMessage(string.Format("{0} confirm stage timed out. Type !upgrade.", yourPlayer.userName));
                    }
                }
                else
                {
                    sendMessage("/w " + user + " You do not have a building yet! Type !newbuilding in the channel chat to obtain a building.");
                }
            }
            else
            {
                sendMessage("/w " + user + " Join the game first by typing !join in the channel chat.");
            }
        }
        else if (lowerMsgString.Equals("!top"))
        {
            List<TwitchPlayer> sortedList = new List<TwitchPlayer>();
            foreach(KeyValuePair<string, TwitchPlayer> twitchPlayer in TwitchPlayerManager.instance.twitchPlayerList)
            {
                sortedList.Add(twitchPlayer.Value);
            }

            // Bubble Sort
            if(sortedList.Count > 1)
            {
                double temp = 0;
                for(int write = 0; write < sortedList.Count; write++)
                {
                    for(int sort = 0; sort < sortedList.Count; sort++)
                    {
                        if(sortedList[sort].yourBuilding.cash > sortedList[sort + 1].yourBuilding.cash)
                        {
                            temp = sortedList[sort + 1].yourBuilding.cash;
                            sortedList[sort+1].yourBuilding.cash = sortedList[sort].yourBuilding.cash;
                            sortedList[sort].yourBuilding.cash = temp;
                        }
                    }
                }
            }

            // Construct string
            string output = "";
            if(sortedList.Count <= 10)
            {
                for(int i = 0; i < sortedList.Count; i++)
                {
                    output += string.Format("({0}: {1} - Cash:{2}) | | ", i+1, sortedList[i].userName, sortedList[i].yourBuilding.cash.ToString("F2"));
                }
            }
            else
            {
                for(int i = 0; i < 10; i++)
                {
                    output += string.Format("({0}: {1} - Cash:{2}) | | ", i+1, sortedList[i].userName, sortedList[i].yourBuilding.cash.ToString("F2"));
                }
            }
            TwitchManager.instance.sendMessage(string.Format("{0} {1}", user, output));
        }
    }

    //when Submit button is clicked or ENTER is pressed.
    public void sendMessage(string message)
    {
        IRC.SendMsg(message);
    }
    Color ColorFromUsername(string username)
    {
        Random.seed = username.Length + (int)username[0] + (int)username[username.Length - 1];
        return new Color(Random.Range(0.25f, 0.55f), Random.Range(0.20f, 0.55f), Random.Range(0.25f, 0.55f));
    }
    // Use this for initialization
    void Start()
    {
        IRC = this.GetComponent<TwitchIRC>();
        //IRC.SendCommand("CAP REQ :twitch.tv/tags"); //register for additional data such as emote-ids, name color etc.
        IRC.messageRecievedEvent.AddListener(OnChatMsgRecieved);
    }
}
