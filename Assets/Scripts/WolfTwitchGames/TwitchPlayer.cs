﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.SerializableAttribute]
public class TwitchPlayer 
{
	public string userName = "No-Name";
	public Color userNameColor = Color.white;
	public Building yourBuilding = null;

	public TwitchPlayer()
	{
		
	}

	public TwitchPlayer(string _userName, Color _userNameColor)
	{
		userName = _userName;
		userNameColor = _userNameColor;
	}
}
