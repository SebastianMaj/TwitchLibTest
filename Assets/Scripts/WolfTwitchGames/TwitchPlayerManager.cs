﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TwitchPlayerManager : MonoBehaviour
{
	public static TwitchPlayerManager instance;

	// List Of Twitch Players
	public Dictionary<string, TwitchPlayer> twitchPlayerList = new Dictionary<string, TwitchPlayer>();

	// Player Added
	public static event Action<TwitchPlayer> PlayerAdded;

	// Player Removed
	public static event Action<TwitchPlayer> PlayerRemoved;

	void Awake()
	{
		if(instance != null && instance != this)
		{
			Destroy(gameObject);
		}

		instance = this;
		DontDestroyOnLoad(gameObject);
	}

	public void addPlayer(TwitchPlayer twitchPlayer)
	{
		if(!twitchPlayerList.ContainsKey(twitchPlayer.userName))
		{
			twitchPlayerList.Add(twitchPlayer.userName, twitchPlayer);
			PlayerAdded(twitchPlayer);

			GameManager.instance.claimRandomBuilding(twitchPlayer.userName);

			TwitchManager.instance.sendMessage(string.Format("{0} has been registered.", twitchPlayer.userName));
		}
	}

	public void removePlayer(TwitchPlayer twitchPlayer)
	{
		if(twitchPlayerList.ContainsKey(twitchPlayer.userName))
		{
			Destroy(twitchPlayerList[twitchPlayer.userName].yourBuilding.gameObject);

			twitchPlayerList.Remove(twitchPlayer.userName);
			PlayerRemoved(twitchPlayer);

			TwitchManager.instance.sendMessage(string.Format("{0} has been removed.", twitchPlayer.userName));
		}
	}

	public static bool isPlayerInGame(string user)
	{
		return TwitchPlayerManager.instance.twitchPlayerList.ContainsKey(user);
	}

	public static TwitchPlayer getPlayer(string user)
	{
		if(isPlayerInGame(user))
			return TwitchPlayerManager.instance.twitchPlayerList[user];
		else
			return null;
	}
}
