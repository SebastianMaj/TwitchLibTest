﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGeneration : MonoBehaviour 
{
	public Vector2 Size = new Vector2(20, 20);
	public static block[,] grid = new block[20, 20];
	public static int worldX, worldY;
	public float Height = 10.0f;
	public float NoiseSize = 10.0f;
	public int seed = 0;

	private GameObject root;
	public GameObject groundObject;

	// Use this for initialization
	void Start () 
	{
		seed = Random.Range(0, 99999);
		grid = new block[(int)Size.x+1, (int)Size.y+1];
		worldX = (int)Size.x+1;
		worldY = (int)Size.y+1;

		Generate();
	}

	public float PerlinNoise(float x, float y, int seed)
	{
		float noise = Mathf.PerlinNoise((seed + x) / NoiseSize, (seed + y) / NoiseSize);

		return noise * Height;
	}

	 //Call this function to generate the terrain
	void Generate ()
	{		
		//If we find a gameobject called terrain, there's a high
		//chance that we have the previous terrain still there
		//So, let's delete it
		Destroy(GameObject.Find("Terrain"));
		
		//Create a new empty gameobject that will store all the objects spawned for extra neatness
		root = new GameObject("Terrain");
		
		//Put the root object at the center of the boxes
		root.transform.position = new Vector3( Size.x/2, 0, Size.y/2 );

		//For loop for x-axis
		for(int i = 0; i <= Size.x; i++)
		{
			
			//For loop for z-axis
			for(int p = 0; p <= Size.y; p++)
			{
				
				GameObject box = groundObject.InstantiateNow();
				
				box.transform.position = new Vector3( i, PerlinNoise( i, p, seed ), p);
				box.transform.parent = root.transform;

				grid[i, p] = new block();
				grid[i, p].groundBlock = box;
				
			}
			
		}
					
		//Move the root at the origin.
		root.transform.position = Vector3.zero;
 	}
}

public class block 
{
	public GameObject groundBlock;

	public GameObject currentBuilding;

	public bool buildBuilding(string userName)
	{
		if(!currentBuilding)
		{
			// Create Building Object
			GameObject buildingToUse = Resources.Load<GameObject>("Buildings/Standard");
			currentBuilding = buildingToUse.InstantiateNow();

			// Get Building Comp
			Building building = currentBuilding.GetComponent<Building>();
			building.owner = TwitchPlayerManager.instance.twitchPlayerList[userName];

			// Set Building in Player
			TwitchPlayerManager.instance.twitchPlayerList[userName].yourBuilding = building;

			// Set Building Position
			Vector3 buildingPos = groundBlock.transform.position; buildingPos.y += 1f;
			currentBuilding.transform.position = buildingPos;

			return true;
		}
		else
		{
			return false;
		}
	}
}
