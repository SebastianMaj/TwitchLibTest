﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager instance;

	void Awake()
	{
		if(instance != null && instance != this)
		{
			Destroy(gameObject);
		}

		instance = this;
	}	

	public void claimRandomBuilding(string user)
	{
		if(TwitchPlayerManager.instance.twitchPlayerList[user].yourBuilding)
			return;

		bool curStatus = false;
		while(!curStatus)
		{
			int x = Random.Range(0, WorldGeneration.worldX);
			int y = Random.Range(0, WorldGeneration.worldY);

			curStatus = buildBuilding(user, x, y);
		}
	}

	bool buildBuilding(string user, int x, int y)
	{
		if(WorldGeneration.grid[x,y].currentBuilding)
			return false;
		else
		{
			WorldGeneration.grid[x,y].buildBuilding(user);
			return true;
		}
	}
}
