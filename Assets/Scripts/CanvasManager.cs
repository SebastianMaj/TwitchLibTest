﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour 
{
	private Dictionary<string, Text> twitchPlayerList = new Dictionary<string, Text>();
	public GameObject playerText;
	public Transform contentParent;

	void NewPlayer(TwitchPlayer twitchPlayer)
	{
		GameObject newPlayerText = playerText.InstantiateNow();
		newPlayerText.transform.SetParent(contentParent);
		newPlayerText.transform.position = Vector3.zero;

		Text plyText = newPlayerText.GetComponent<Text>();
		plyText.text = string.Format("<color={0}>{1}</color>", twitchPlayer.userNameColor.ColorCode(), twitchPlayer.userName);

		twitchPlayerList.Add(twitchPlayer.userName, plyText);
	}

	void RemovePlayer(TwitchPlayer twitchPlayer)
	{
		if(twitchPlayerList.ContainsKey(twitchPlayer.userName))
		{
			Destroy(twitchPlayerList[twitchPlayer.userName].gameObject);
			twitchPlayerList.Remove(twitchPlayer.userName);
		}
	}

	// Callbacks
	void OnEnable()
	{
		TwitchPlayerManager.PlayerAdded += NewPlayer;
		TwitchPlayerManager.PlayerRemoved += RemovePlayer;
	}	

	void OnDisable()
	{
		TwitchPlayerManager.PlayerAdded -= NewPlayer;
		TwitchPlayerManager.PlayerRemoved -= RemovePlayer;
	}
}
